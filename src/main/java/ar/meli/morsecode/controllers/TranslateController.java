package ar.meli.morsecode.controllers;

import ar.meli.morsecode.models.HumanToMorse;
import ar.meli.morsecode.models.MorseToHuman;
import ar.meli.morsecode.models.TranslateResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TranslateController {

    @RequestMapping("/translate/2text")
    public TranslateResponse translate2Text(@RequestParam(value="text") String text) {
        MorseToHuman morseToHuman = new MorseToHuman();
        String humanText = morseToHuman.translate2Human(text);
        return new TranslateResponse(humanText);
    }

    @RequestMapping("/translate/2morse")
    public TranslateResponse translate2Morse(@RequestParam(value="text") String text) {
        HumanToMorse humanToMorse = new HumanToMorse();
        String morseText = humanToMorse.translate2Morse(text);
        return new TranslateResponse(morseText);
    }
}