package ar.meli.morsecode.exceptions;

public class InvalidBitStringException extends Exception {

    public InvalidBitStringException() {
        super("Invalid bit string Exception!");
    }
}
