package ar.meli.morsecode.exceptions;

public class EmptyStringException extends Exception {

    public EmptyStringException() {
        super("Empty String Exception!");
    }
}
