package ar.meli.morsecode.models;

public class MorseSymbol7 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol8();
    }

    @Override
    public String getMorse() {
        return "--...";
    }

    @Override
    public String getHumanLanguage() {
        return "7";
    }
}