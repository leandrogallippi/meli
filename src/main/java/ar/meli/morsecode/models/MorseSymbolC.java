package ar.meli.morsecode.models;

public class MorseSymbolC extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolD();
    }

    @Override
    public String getMorse() {
        return "-.-.";
    }

    @Override
    public String getHumanLanguage() {
        return "C";
    }
}