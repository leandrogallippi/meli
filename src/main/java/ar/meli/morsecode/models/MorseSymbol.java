package ar.meli.morsecode.models;

public abstract class MorseSymbol {

    public abstract MorseSymbol next();

    public abstract String getMorse();

    public abstract String getHumanLanguage();

    public String translate2Human(String morseText) {
        return (morseText.equals(getMorse())) ? getHumanLanguage() : next().translate2Human(morseText);
    }

    public String translate2Morse(String humanText) {
        return (humanText.equals(getHumanLanguage())) ? getMorse() : next().translate2Morse(humanText);
    }
}
