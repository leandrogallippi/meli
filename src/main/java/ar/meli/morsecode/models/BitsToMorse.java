package ar.meli.morsecode.models;

import ar.meli.morsecode.exceptions.InvalidBitStringException;

public class BitsToMorse {

    /*private void checkLengthOfBits(String bits) throws EmptyStringException {
        if (bits.length() == 0) {
            throw new EmptyStringException();
        }
    }*/

    private BitsSequencesContainer createBitsSequencesContainer(String bits) {
        BitsSequencesContainer bitsSequencesContainer = new BitsSequencesContainer();

        int bitsSequenceLength = 0;
        Bit lastBit = null;

        for (int i = 0; i < bits.length(); i++){

            try {
                Bit currentBit = new Bit(bits.charAt(i));
                if (lastBit != null && (!lastBit.equals(currentBit) || i == bits.length() - 1)) {

                    bitsSequencesContainer.addBitsSequence(lastBit, bitsSequenceLength);

                    bitsSequenceLength = 0;
                }

                lastBit = currentBit.copyBit();
                bitsSequenceLength ++;

            }
            catch (InvalidBitStringException e) {
                return null;
            }
        }

        return bitsSequencesContainer;
    }


    public String decodeBits2Morse(String bits) {

        BitsSequencesContainer bitsSequencesContainer = this.createBitsSequencesContainer(bits);

        if (bitsSequencesContainer == null) {
            return "";
        }

        //return bitsSequencesContainer.toString();

        StringBuilder morseBuilder = new StringBuilder();

        for (BitsSequence bitsSequence: bitsSequencesContainer.getBitsSequences()) {
            morseBuilder.append(this.decodeBitsSequence2Morse(bitsSequence, bitsSequencesContainer));
        }

        return morseBuilder.toString().trim();
    }

    private String decodeBitsSequence2Morse(BitsSequence bitsSequence, BitsSequencesContainer bitsSequencesContainer) {
        if (bitsSequence.isPause()) {
            return this.decodePause2Morse(bitsSequencesContainer, bitsSequence.getLength());
        }
        return this.decodePulse2Morse(bitsSequencesContainer, bitsSequence.getLength());
    }

    private String decodePause2Morse(BitsSequencesContainer bitsSequencesContainer, int pauseLength) {
        return (pauseLength < bitsSequencesContainer.getRateLengthOfPause() - 1) ? "" : " ";
    }

    private String decodePulse2Morse(BitsSequencesContainer bitsSequencesContainer, int pulseLength) {
        return (pulseLength < bitsSequencesContainer.getRateLengthOfPulse() - 1) ? "." : "-";
    }
}
