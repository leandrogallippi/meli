package ar.meli.morsecode.models;

public class MorseSymbol8 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol9();
    }

    @Override
    public String getMorse() {
        return "---..";
    }

    @Override
    public String getHumanLanguage() {
        return "8";
    }
}