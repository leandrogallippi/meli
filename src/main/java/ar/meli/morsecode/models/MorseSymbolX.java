package ar.meli.morsecode.models;

public class MorseSymbolX extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolY();
    }

    @Override
    public String getMorse() {
        return "-..-";
    }

    @Override
    public String getHumanLanguage() {
        return "Z";
    }
}