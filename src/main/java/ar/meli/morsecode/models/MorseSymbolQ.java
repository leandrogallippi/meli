package ar.meli.morsecode.models;

public class MorseSymbolQ extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolR();
    }

    @Override
    public String getMorse() {
        return "--.-";
    }

    @Override
    public String getHumanLanguage() {
        return "Q";
    }
}