package ar.meli.morsecode.models;

public class MorseSymbolJ extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolK();
    }

    @Override
    public String getMorse() {
        return ".---";
    }

    @Override
    public String getHumanLanguage() {
        return "J";
    }
}