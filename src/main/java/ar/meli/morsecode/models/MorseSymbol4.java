package ar.meli.morsecode.models;

public class MorseSymbol4 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol5();
    }

    @Override
    public String getMorse() {
        return "....-";
    }

    @Override
    public String getHumanLanguage() {
        return "4";
    }
}