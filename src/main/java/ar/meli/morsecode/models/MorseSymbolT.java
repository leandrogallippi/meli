package ar.meli.morsecode.models;

public class MorseSymbolT extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolU();
    }

    @Override
    public String getMorse() {
        return "-";
    }

    @Override
    public String getHumanLanguage() {
        return "T";
    }
}