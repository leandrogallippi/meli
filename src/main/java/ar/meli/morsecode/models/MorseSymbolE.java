package ar.meli.morsecode.models;

public class MorseSymbolE extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolF();
    }

    @Override
    public String getMorse() {
        return ".";
    }

    @Override
    public String getHumanLanguage() {
        return "E";
    }
}