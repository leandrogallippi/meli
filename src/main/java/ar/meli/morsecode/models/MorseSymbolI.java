package ar.meli.morsecode.models;

public class MorseSymbolI extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolJ();
    }

    @Override
    public String getMorse() {
        return "..";
    }

    @Override
    public String getHumanLanguage() {
        return "I";
    }
}