package ar.meli.morsecode.models;

public class MorseAlphabet {

    MorseSymbol firstMorseSymbol;

    public MorseAlphabet() {
        firstMorseSymbol = new MorseSymbolA();
    }

    public String translate2Human(String morseText) {
        return firstMorseSymbol.translate2Human(morseText);
    }

    public String translate2Morse(String humanText) {
        return firstMorseSymbol.translate2Morse(humanText);
    }
}
