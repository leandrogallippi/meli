package ar.meli.morsecode.models;


public class MorseSymbolFullStop extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return null;
    }

    @Override
    public String getMorse() {
        return "asd";
    }

    @Override
    public String getHumanLanguage() {
        return "asd";
    }

    @Override
    public String translate2Human(String morseText) {
        return getHumanLanguage();
    }

    @Override
    public String translate2Morse(String humanText) {
        return getMorse();
    }
}