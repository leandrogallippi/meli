package ar.meli.morsecode.models;

public class MorseSymbolA extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolB();
    }

    @Override
    public String getMorse() {
        return ".-";
    }

    @Override
    public String getHumanLanguage() {
        return "A";
    }
}
