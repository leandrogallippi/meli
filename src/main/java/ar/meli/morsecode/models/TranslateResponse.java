package ar.meli.morsecode.models;

public class TranslateResponse {

    private int code;

    private String response;

    public TranslateResponse(String response) {
        this.response = response;

        if (response.equals("")) {
            this.code = 500;
            this.response = "Invalid text";
        }
        else {
            this.code = 200;
            this.response = response;
        }
    }

    public int getCode() {
        return code;
    }

    public String getResponse() {
        return response;
    }
}
