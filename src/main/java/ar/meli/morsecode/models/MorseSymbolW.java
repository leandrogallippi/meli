package ar.meli.morsecode.models;

public class MorseSymbolW extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolX();
    }

    @Override
    public String getMorse() {
        return ".--";
    }

    @Override
    public String getHumanLanguage() {
        return "W";
    }
}