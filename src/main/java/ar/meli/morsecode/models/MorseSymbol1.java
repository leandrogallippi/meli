package ar.meli.morsecode.models;

public class MorseSymbol1 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol2();
    }

    @Override
    public String getMorse() {
        return ".---";
    }

    @Override
    public String getHumanLanguage() {
        return "1";
    }
}