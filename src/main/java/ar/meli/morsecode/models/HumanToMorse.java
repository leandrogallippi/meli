package ar.meli.morsecode.models;

public class HumanToMorse {

    private MorseAlphabet morseAlphabet;

    public HumanToMorse() {
        morseAlphabet = new MorseAlphabet();
    }

    public String translate2Morse(String humanString) {

        StringBuilder morseTextBuilder = new StringBuilder();

        for (int i = 0; i < humanString.length(); i++) {
            Character humanChar = humanString.charAt(i);
            String humanSymbol = humanChar.toString();
            morseTextBuilder.append(morseAlphabet.translate2Morse(humanChar.toString()));
            if (i != humanString.length() - 1) {
                morseTextBuilder.append(" ");
            }
        }

        return morseTextBuilder.toString();
    }
}
