package ar.meli.morsecode.models;

public class MorseSymbolK extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolL();
    }

    @Override
    public String getMorse() {
        return "-.-";
    }

    @Override
    public String getHumanLanguage() {
        return "K";
    }
}