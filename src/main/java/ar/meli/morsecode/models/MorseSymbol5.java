package ar.meli.morsecode.models;

public class MorseSymbol5 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol6();
    }

    @Override
    public String getMorse() {
        return ".....";
    }

    @Override
    public String getHumanLanguage() {
        return "5";
    }
}