package ar.meli.morsecode.models;

public class MorseSymbolS extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolT();
    }

    @Override
    public String getMorse() {
        return "...";
    }

    @Override
    public String getHumanLanguage() {
        return "S";
    }
}