package ar.meli.morsecode.models;

public class MorseSymbolV extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolW();
    }

    @Override
    public String getMorse() {
        return "...-";
    }

    @Override
    public String getHumanLanguage() {
        return "V";
    }
}