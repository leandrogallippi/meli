package ar.meli.morsecode.models;

public class MorseSymbolY extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolZ();
    }

    @Override
    public String getMorse() {
        return "-.--";
    }

    @Override
    public String getHumanLanguage() {
        return "Y";
    }
}