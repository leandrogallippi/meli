package ar.meli.morsecode.models;

public class BitsSequence {

    private Bit bit;

    private Integer length;

    public BitsSequence(Bit bit, Integer length) {
        this.bit = bit;
        this.length = length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("BIT: ");
        stringBuilder.append(bit);
        stringBuilder.append(" - LARGO: ");
        stringBuilder.append(length);
        return stringBuilder.toString();
    }

    public Bit getBit() {
        return bit;
    }

    public Integer getLength() {
        return length;
    }

    public boolean isPause() {
        return bit.isPause();
    }

    public boolean isPulse() {
        return bit.isPulse();
    }



}
