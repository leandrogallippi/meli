package ar.meli.morsecode.models;

public class MorseSymbol9 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolFullStop();
    }

    @Override
    public String getMorse() {
        return "----.";
    }

    @Override
    public String getHumanLanguage() {
        return "9";
    }
}