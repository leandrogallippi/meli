package ar.meli.morsecode.models;

public class MorseSymbol3 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol4();
    }

    @Override
    public String getMorse() {
        return "...--";
    }

    @Override
    public String getHumanLanguage() {
        return "3";
    }
}