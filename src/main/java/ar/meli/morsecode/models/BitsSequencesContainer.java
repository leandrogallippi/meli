package ar.meli.morsecode.models;
import java.util.*;

public class BitsSequencesContainer {

    private ArrayList<BitsSequence> bitsSequences;

    private Integer minLengthOfPause = 0;

    private Integer maxLengthOfPause = 1;

    private Integer minLengthOfPulse = 0;

    private Integer maxLengthOfPulse = 1;

    public BitsSequencesContainer() {
        bitsSequences = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(bitsSequences.toString());
        stringBuilder.append(",RATE LENGTH OF PAUSE:");
        stringBuilder.append(getRateLengthOfPause());
        stringBuilder.append(",RATE LENGTH OF PULSE:");
        stringBuilder.append(getRateLengthOfPulse());
        return stringBuilder.toString();
    }

    public Integer getRateLengthOfPause() {
        return (minLengthOfPause + maxLengthOfPause) / 2;
    }

    public Integer getRateLengthOfPulse() {
        return (minLengthOfPulse + maxLengthOfPulse) / 2;
    }

    private boolean mustUpdateMinLength(Integer minLength, int newLength) {
        return (newLength < minLength || minLength == 0);
    }

    private boolean mustUpdateMaxLength(Integer maxLength, int newLength) {
        return (newLength > maxLength);
    }

    public void addBitsSequence(Bit bit, int length) {
        BitsSequence bitsSequence = new BitsSequence(bit, length);
        bitsSequences.add(bitsSequence);
        if (bit.isPause()) {
            if (mustUpdateMinLength(minLengthOfPause, length)) {
                minLengthOfPause = length;
            }
            if (mustUpdateMaxLength(maxLengthOfPause, length)) {
                maxLengthOfPause = length;
            }

        }
        else {
            if (mustUpdateMinLength(minLengthOfPulse, length)) {
                minLengthOfPulse = length;
            }
            if (mustUpdateMaxLength(maxLengthOfPulse, length)) {
                maxLengthOfPulse = length;
            }
        }

    }

    public ArrayList<BitsSequence> getBitsSequences() {
        return bitsSequences;
    }
}
