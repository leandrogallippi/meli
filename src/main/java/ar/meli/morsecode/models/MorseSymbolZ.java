package ar.meli.morsecode.models;

public class MorseSymbolZ extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolWhiteSpace();
    }

    @Override
    public String getMorse() {
        return "--..";
    }

    @Override
    public String getHumanLanguage() {
        return "Z";
    }
}