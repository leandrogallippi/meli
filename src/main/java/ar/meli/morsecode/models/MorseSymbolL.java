package ar.meli.morsecode.models;

public class MorseSymbolL extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolM();
    }

    @Override
    public String getMorse() {
        return ".-..";
    }

    @Override
    public String getHumanLanguage() {
        return "L";
    }
}