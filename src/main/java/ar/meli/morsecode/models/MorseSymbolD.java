package ar.meli.morsecode.models;

public class MorseSymbolD extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolE();
    }

    @Override
    public String getMorse() {
        return "-..";
    }

    @Override
    public String getHumanLanguage() {
        return "D";
    }
}