package ar.meli.morsecode.models;

public class MorseSymbolG extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolH();
    }

    @Override
    public String getMorse() {
        return "--.";
    }

    @Override
    public String getHumanLanguage() {
        return "G";
    }
}