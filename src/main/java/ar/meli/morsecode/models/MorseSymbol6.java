package ar.meli.morsecode.models;

public class MorseSymbol6 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol7();
    }

    @Override
    public String getMorse() {
        return "-....";
    }

    @Override
    public String getHumanLanguage() {
        return "6";
    }
}