package ar.meli.morsecode.models;

import ar.meli.morsecode.exceptions.InvalidBitStringException;

public class Bit {


    Character bit;

    @Override
    public String toString() {
        return bit.toString();
    }

    public Bit(Character bit) throws InvalidBitStringException{
        this.bit = bit;
        if (!this.isValidBit()) {
            throw new InvalidBitStringException();
        }
    }

    public Character getBit() {
        return bit;
    }

    /**
     *
     * @return boolean
     */
    public boolean isPause() {
        return bit == '0';
    }

    /**
     *
     * @return boolean
     */
    public boolean isPulse() {
        return bit == '1';
    }

    /**
     *
     * @return
     */
    public boolean isValidBit() {
        return isPulse() || isPause();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Bit)) {
            return false;
        }
        return this.getBit() == ((Bit) o).getBit();
    }

    public Bit copyBit() throws InvalidBitStringException {
        return new Bit(this.bit);
    }
}
