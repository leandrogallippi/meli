package ar.meli.morsecode.models;

public class MorseSymbolR extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolS();
    }

    @Override
    public String getMorse() {
        return ".-.";
    }

    @Override
    public String getHumanLanguage() {
        return "R";
    }
}