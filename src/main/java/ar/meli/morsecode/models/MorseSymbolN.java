package ar.meli.morsecode.models;

public class MorseSymbolN extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolO();
    }

    @Override
    public String getMorse() {
        return "-.";
    }

    @Override
    public String getHumanLanguage() {
        return "N";
    }
}