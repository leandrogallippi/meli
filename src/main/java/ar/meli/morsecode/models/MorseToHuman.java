package ar.meli.morsecode.models;

public class MorseToHuman {

    private MorseAlphabet morseAlphabet;

    public MorseToHuman() {
        morseAlphabet = new MorseAlphabet();
    }

    private String[] splitMorseString(String morseString) {
        return morseString.trim().split(" ");
    }

    public String translate2Human(String morseString) {

        String[] morseCodes = this.splitMorseString(morseString);

        StringBuilder humanTextBuilder = new StringBuilder();

        for (String morseCode: morseCodes) {
            morseCode = (! morseCode.equals("")) ? morseCode : " ";
            humanTextBuilder.append(morseAlphabet.translate2Human(morseCode));
        }

        return humanTextBuilder.toString().replaceAll("\\s+", " ");
    }
}
