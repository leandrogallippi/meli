package ar.meli.morsecode.models;

public class MorseSymbolB extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolC();
    }

    @Override
    public String getMorse() {
        return "-...";
    }

    @Override
    public String getHumanLanguage() {
        return "B";
    }
}