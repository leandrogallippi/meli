package ar.meli.morsecode.models;

public class MorseSymbolF extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolG();
    }

    @Override
    public String getMorse() {
        return "..-.";
    }

    @Override
    public String getHumanLanguage() {
        return "F";
    }
}