package ar.meli.morsecode.models;

public class MorseSymbol0 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol1();
    }

    @Override
    public String getMorse() {
        return "----";
    }

    @Override
    public String getHumanLanguage() {
        return "0";
    }
}