package ar.meli.morsecode.models;

public class MorseSymbolU extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolV();
    }

    @Override
    public String getMorse() {
        return "..-";
    }

    @Override
    public String getHumanLanguage() {
        return "U";
    }
}