package ar.meli.morsecode.models;

public class MorseSymbolH extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolI();
    }

    @Override
    public String getMorse() {
        return "....";
    }

    @Override
    public String getHumanLanguage() {
        return "H";
    }
}