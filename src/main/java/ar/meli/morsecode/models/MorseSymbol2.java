package ar.meli.morsecode.models;

public class MorseSymbol2 extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol3();
    }

    @Override
    public String getMorse() {
        return "..---";
    }

    @Override
    public String getHumanLanguage() {
        return "2";
    }
}