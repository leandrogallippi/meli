package ar.meli.morsecode.models;

public class MorseSymbolWhiteSpace extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbol0();
    }

    @Override
    public String getMorse() {
        return " ";
    }

    @Override
    public String getHumanLanguage() {
        return " ";
    }
}