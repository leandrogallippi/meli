package ar.meli.morsecode.models;

public class MorseSymbolM extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolN();
    }

    @Override
    public String getMorse() {
        return "--";
    }

    @Override
    public String getHumanLanguage() {
        return "M";
    }
}
