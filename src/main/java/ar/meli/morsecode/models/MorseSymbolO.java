package ar.meli.morsecode.models;

public class MorseSymbolO extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolP();
    }

    @Override
    public String getMorse() {
        return "---";
    }

    @Override
    public String getHumanLanguage() {
        return "O";
    }
}