package ar.meli.morsecode.models;

public class MorseSymbolP extends MorseSymbol {

    @Override
    public MorseSymbol next() {
        return new MorseSymbolQ();
    }

    @Override
    public String getMorse() {
        return ".--.";
    }

    @Override
    public String getHumanLanguage() {
        return "P";
    }
}