import ar.meli.morsecode.models.HumanToMorse;
import org.junit.Test;
import static org.junit.Assert.*;

public class HumanToMorseTest {

    HumanToMorse humanToMorse;

    public HumanToMorseTest() {
        humanToMorse = new HumanToMorse();
    }

    @Test
    public void testTranslate2HumanIsRight() throws Exception {
        assertEquals(".... --- .-.. .-   -- . .-.. ..", humanToMorse.translate2Morse("HOLA MELI"));
    }

}
