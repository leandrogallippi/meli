import org.junit.Test;
import static org.junit.Assert.*;
import ar.meli.morsecode.models.MorseAlphabet;

public class MorseAlphabetTest {

    MorseAlphabet morseAlphabet;

    public MorseAlphabetTest() {
        morseAlphabet = new MorseAlphabet();
    }

    @Test
    public void testTranslate2HumanSymbolAIsRight() throws Exception {
        assertEquals("A", morseAlphabet.translate2Human(".-"));
    }

    @Test
    public void testTranslate2HumanSymbolHIsRight() throws Exception {
        assertEquals("O", morseAlphabet.translate2Human("---"));
    }

    @Test
    public void testTranslate2HumanSymboHIsRight() throws Exception {
        assertEquals("H", morseAlphabet.translate2Human("...."));
    }

    @Test
    public void testTranslate2HumanSymbolLIsRight() throws Exception {
        assertEquals("L", morseAlphabet.translate2Human(".-.."));
    }

    @Test
    public void testTranslate2HumanSymbolMIsRight() throws Exception {
        assertEquals("M", morseAlphabet.translate2Human("--"));
    }

    @Test
    public void testTranslate2HumanSymbolEIsRight() throws Exception {
        assertEquals("E", morseAlphabet.translate2Human("."));
    }

    @Test
    public void testTranslate2HumanSymbolIIsRight() throws Exception {
        assertEquals("I", morseAlphabet.translate2Human(".."));
    }

    @Test
    public void testTranslate2HumanSymbolWhiteSpaceIsRight() throws Exception {
        assertEquals(" ", morseAlphabet.translate2Human(" "));
    }

    @Test
    public void testTranslate2MorseSymbolAIsRight() throws Exception {
        assertEquals(".-", morseAlphabet.translate2Morse("A"));
    }

    @Test
    public void testTranslate2MorseSymbolOIsRight() throws Exception {
        assertEquals("---", morseAlphabet.translate2Morse("O"));
    }

    @Test
    public void testTranslate2MorseSymbolHIsRight() throws Exception {
        assertEquals("....", morseAlphabet.translate2Morse("H"));
    }

    @Test
    public void testTranslate2MorseSymbolLIsRight() throws Exception {
        assertEquals(".-..", morseAlphabet.translate2Morse("L"));
    }

    @Test
    public void testTranslate2MorseSymbolMIsRight() throws Exception {
        assertEquals("--", morseAlphabet.translate2Morse("M"));
    }

    @Test
    public void testTranslate2MorseSymbolEIsRight() throws Exception {
        assertEquals(".", morseAlphabet.translate2Morse("E"));
    }

    @Test
    public void testTranslate2MorseSymbolIIsRight() throws Exception {
        assertEquals("..", morseAlphabet.translate2Morse("I"));
    }

    @Test
    public void testTranslate2MorseSymbolWhiteSpaceIsRight() throws Exception {
        assertEquals(" ", morseAlphabet.translate2Morse(" "));
    }

}
