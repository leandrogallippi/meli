import org.junit.Test;
import static org.junit.Assert.*;
import ar.meli.morsecode.models.MorseToHuman;

public class MorseToHumanTest {

    MorseToHuman morseDecoder;

    public MorseToHumanTest() {
        morseDecoder = new MorseToHuman();
    }

    @Test
    public void testTranslate2HumanIsRight() throws Exception {
        assertEquals("HOLA MELI", morseDecoder.translate2Human(".... --- .-.. .-     -- . .-.. .."));
    }

}
