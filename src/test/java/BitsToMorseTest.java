import org.junit.Test;
import static org.junit.Assert.*;
import ar.meli.morsecode.models.BitsToMorse;

public class BitsToMorseTest {

    BitsToMorse morseEncoder;

    public BitsToMorseTest() {
        morseEncoder = new BitsToMorse();
    }

    @Test
    public void testDecodeBits2MorseIsRight() throws Exception {
        assertEquals(".... --- .-.. .- -- . .-.. ..", morseEncoder.decodeBits2Morse("000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000"));
    }

}
